<?php
namespace app\home\controller;
use think\Controller;
use think\Session;
use think\Db;

class Index extends Controller
{
    public function index()
    {
    	$uid=session('mid');
    	if(!$uid){
    		$this->redirect('Account/login');
    	}
    	//求出该用户的所有权限
    	$roleInfo=Db::table('manager')->where('id',$uid)->find();
    	$this->assign('username',$roleInfo['username']);
    	//求出该用户所有的role_auth_ids
    	$ids=Db::table('role')->where('role_id',$roleInfo['role_id'])->find();
    	//得到对应的权限信息
    	$idRow=explode(',',$ids['role_auth_ids']);
    	//得到父级权限
    	$fathers=Db::table('auth')->where('auth_id','in',$idRow)->where('auth_level',0)->select();
    	//得到子级权限
    	$sons=Db::table('auth')->where('auth_id','in',$idRow)->where('auth_level',1)->select();
    	$this->assign('fathers',$fathers);
    	$this->assign('sons',$sons);	
      return $this->view->fetch('Index/index');
    }
}
